#!bin/bash/
##https://computingforgeeks.com/install-influxdb-on-ubuntu-and-debian/
#Add GPG Key
sudo apt update
sudo apt install -y curl wget
##jout du repo
echo "deb https://repos.influxdata.com/ubuntu focal stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
sudo curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
##installation InfluxDB
sudo apt-get update
sudo apt-get install -y influxdb2
dpkg -L influxdb2
##gerer le service
sudo systemctl start influxdb
sudo systemctl enable influxdb
##Setup InfluxDB
influx setup
####cat<<EOF
#> Welcome to InfluxDB 2.0!
#? Please type your primary username cyberithub
#? Please type your password *********
#? Please type your password again *********
#? Please type your primary organization name CyberITHub
#? Please type your primary bucket name cyberithub_bucket
#? Please type your retention period in hours, or 0 for infinite 0
#? Setup with these parameters?
#Username: cyberithub
#Organization: CyberITHub
#Bucket: cyberithub_bucket
#Retention Period: infinite
#Yes
#User Organization Bucket
#cyberithub CyberITHub cyberithub_bucket
#EOF
##creation de l'utilisateur
##influx user create --name test --org CyberITHub --password Test@123$
##creation de role "Create User Authorization"
##influx auth create --user test --read-buckets --write-buckets
sudo apt-get install ufw
sudo ufw enable
sudo ufw allow 8086/tcp
##InfluxDB http Authentication (optional)

